#include <common>
#include <logdepthbuf_pars_vertex>

attribute vec3 barycentric;
attribute float even;
attribute vec3 color;

varying vec3 vBarycentric;

varying vec3 vPosition;
varying float vEven;
varying vec2 vUv;
varying vec3 vColor;

void main() {
    #include <begin_vertex>
    #include <project_vertex>
    vBarycentric = barycentric;
    vPosition = position.xyz;
    vEven = even;
    vUv = uv;
    vColor = color;

    #include <logdepthbuf_vertex>
}
