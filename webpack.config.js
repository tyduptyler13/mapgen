// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WorkboxWebpackPlugin = require("workbox-webpack-plugin");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const ReactRefreshPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const ReactRefreshTypescript = require("react-refresh-typescript");

const isProduction = process.env.NODE_ENV === "production";

const stylesHandler = "style-loader";

module.exports = (env) =>
  /**
   * @type {import('webpack').Configuration}
   */
  ({
    mode: isProduction ? "production" : "development",
    entry: ["./src/index.tsx"],
    output: {
      path: path.resolve(__dirname, "dist"),
      clean: true,
    },
    devtool: isProduction ? "source-map" : "inline-source-map",
    devServer: {
      open: true,
      host: "localhost",
      hot: true,
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "index.html",
      }),
      ...(isProduction
        ? [
            new BundleAnalyzerPlugin({
              analyzerMode: "static",
              reportFilename: path.resolve(__dirname, "stats/index.html"),
              openAnalyzer: false,
            }),
            new WorkboxWebpackPlugin.GenerateSW(),
          ]
        : [new ReactRefreshPlugin()]),

      // Add your plugins here
      // Learn more about plugins from https://webpack.js.org/configuration/plugins/
    ],
    module: {
      rules: [
        {
          test: /\.tsx?$/i,
          exclude: [/node_modules/],
          use: [
            {
              loader: "ts-loader",
              options: {
                projectReferences: true,
                getCustomTransformers: () => ({
                  before: isProduction ? [] : [ReactRefreshTypescript()],
                }),
              },
            },
          ],
        },
        {
          test: /\.s[ac]ss$/i,
          use: [stylesHandler, "css-loader", "sass-loader"],
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
          type: "asset",
        },
        {
          test: /\.(frag|vert|glsl)$/i,
          use: "@tyduptyler13/glslify-loader",
          type: "asset/source",
        },
        {
          test: /\.js$/,
          enforce: "pre",
          use: "source-map-loader",
        },

        // Add your rules for custom modules here
        // Learn more about loaders from https://webpack.js.org/loaders/
      ],
    },
    resolve: {
      extensions: [".tsx", ".ts", ".jsx", ".js", "..."],
      alias: {
        "#": path.resolve(__dirname, "static"),
        "@": path.resolve(__dirname, "src"),
        "@glsl": path.resolve(__dirname, "glsl"),
        "@worker": path.resolve(__dirname, "worker/build/src"),
      },
    },
  });
