import { Seed } from "./random";
import { makeNoise2D, makeNoise4D } from "open-simplex-noise";
import { AltitudeScales } from "../types";
import { MersenneTwister } from "./random/MersenneTwister";
import { interpolate } from "../math/interpolate";

const altitudeFunction = interpolate(
  [
    [1.0, 1.0],
    [0.9, 0.2],
    [0.8, 0.1],
    [0.7, 0.0],
    [0.6, -0.01],
    [0.5, -0.3],
    [0.4, -0.4],
    [0.3, -0.5],
    [0.2, -0.6],
    [0.1, -0.65],
    [0.0, -1.0],
  ],
  7,
);

/**
 * Returns an approximation of the altitude given a quotient aka the hypsometric curve
 * <p>
 * See https://serc.carleton.edu/mathyouneed/hypsometric/index.html
 * <p>
 *  Estimated using `degree 7 fit {{1.0,1}, {.9,.2}, {.8,.1}, {.7,0}, {.6,-.01}, {.5,-.3}, {.4,-.4}, {.3,-.5}, {.2,-.6}, {.1,-.65}, {0,-1}}`
 *
 * * R ** 2 = .996485
 * <p>
 * Output assumes water is at 0 and altitude ranges from 6 to -11 ish
 *
 * @param x a value between [0 and 1] inclusive.
 */
export function altitudeApproximation(x: number): number {
  return altitudeFunction(x);
}

// TODO try using fft? or maybe a higher order polynomial with more samples if we find that this result isn't satisfying.

// function sCurve(x: number, amp: number = 2) {
//   return 1 - 1 / (1 + (x / (1 - x)) ** amp);
// }

export function makeAltitudeGenerator(seed: Seed = Math.random()) {
  const prng = new MersenneTwister(seed);

  // @ts-ignore
  global.prng = prng;

  const minorNoiseGen = makeNoise2D(prng.random_int());
  const majorNoiseGen = makeNoise2D(prng.random_int());
  const majorNoise2Gem = makeNoise2D(prng.random_int());
  const distortionX = makeNoise2D(prng.random_int());
  const distortionY = makeNoise2D(prng.random_int());

  return function applyAltitude(
    positionBuffer: Float32Array,
    {
      minorScale = 4.0,
      majorScale = 10.0,
      distortionScale = 4.0,
    }: AltitudeScales = {
      minorScale: 4.0,
      majorScale: 10.0,
      distortionScale: 4.0,
    },
  ) {
    const size = positionBuffer.length / 3;
    const minorNoise = new Float32Array(size);
    const majorNoise = new Float32Array(size);
    const majorNoise2 = new Float32Array(size);

    for (let i = 0; i < size; ++i) {
      const x = positionBuffer[i * 3];
      const y = positionBuffer[i * 3 + 2];

      const dx = distortionX(x / distortionScale, y / distortionScale);
      const dy = distortionY(x / distortionScale, y / distortionScale);

      const minor = minorNoiseGen(x / minorScale, y / minorScale);
      const major = majorNoiseGen((x + dx) / majorScale, (y + dy) / majorScale);
      const major2 = majorNoise2Gem(
        (x + dx) / majorScale,
        (y + dy) / majorScale,
      );

      let normalMinor = (minor + 1) / 2; // [0, 1] but is more of a normal distribution
      let normalMajor = (major + 1) / 2; // [0, 1] but is more of a normal distribution
      let normalMajor2 = (major2 + 1) / 2; // [0, 1] but is more of a normal distribution

      minorNoise[i] = normalMinor;
      majorNoise[i] = normalMajor;
      majorNoise2[i] = normalMajor2;

      let z = Math.max(-1, Math.min(1, major * 3));

      z = Math.min(z, (major2 + 1) * 3 - 1);

      let roughness = 1 - (1 - normalMinor) ** 4;
      z = (z * roughness + 1) / 2;

      z = altitudeApproximation(z);
      // z = z * 2 - 1
      // z *= 5

      // let z = major;

      positionBuffer[i * 3 + 1] = z;
    }

    return {
      positionBuffer,
      minorNoise,
      majorNoise,
      majorNoise2,
    };
  };
}

export function makeLoopingAltitudeGenerator(seed: Seed = Math.random()) {
  const prng = new MersenneTwister(seed);

  // @ts-ignore
  global.prng = prng;

  const minorNoiseGen = makeNoise4D(prng.random_int());
  const majorNoiseGen = makeNoise4D(prng.random_int());
  const majorNoise2Gen = makeNoise4D(prng.random_int());
  const distortionNoiseX = makeNoise2D(prng.random_int());
  const distortionNoiseY = makeNoise2D(prng.random_int());

  return function applyAltitude(
    positionBuffer: Float32Array,
    maxWidth: number,
    maxHeight: number,
    {
      minorScale = 4.0,
      majorScale = 10.0,
      distortionScale = 4.0,
    }: AltitudeScales = {
      minorScale: 4.0,
      majorScale: 10.0,
      distortionScale: 4.0,
    },
  ) {
    const size = positionBuffer.length / 3;
    const minorNoise = new Float32Array(size);
    const majorNoise = new Float32Array(size);
    const majorNoise2 = new Float32Array(size);
    const height = new Float32Array(size);

    for (let i = 0; i < size; ++i) {
      const x = positionBuffer[i * 3];
      const y = positionBuffer[i * 3 + 2];
      const s = x / maxWidth;
      const t = y / maxHeight;

      const dx = (Math.cos(s * 2 * Math.PI) * maxWidth) / (2 * Math.PI);
      const dy = (Math.cos(t * 2 * Math.PI) * maxHeight) / (2 * Math.PI);

      const distortionX = distortionNoiseX(
        dx / distortionScale,
        dy / distortionScale,
      );
      const distortionY = distortionNoiseY(
        dx / distortionScale,
        dy / distortionScale,
      );

      const dz =
        (Math.sin((s + distortionX / maxWidth) * 2 * Math.PI) * maxWidth) /
        (2 * Math.PI);
      const dw =
        (Math.sin((t + distortionY / maxHeight) * 2 * Math.PI) * maxHeight) /
        (2 * Math.PI);

      const minor = minorNoiseGen(
        dx / minorScale,
        dy / minorScale,
        dz / minorScale,
        dw / minorScale,
      );
      const major = majorNoiseGen(
        dx / majorScale,
        dy / majorScale,
        dz / majorScale,
        dw / majorScale,
      );
      const major2 = majorNoise2Gen(
        dx / majorScale,
        dy / majorScale,
        dz / majorScale,
        dw / majorScale,
      );
      //   (x + dz) / majorScale,
      // );

      let normalMinor = (minor + 1) / 2; // [0, 1] but is more of a normal distribution
      let normalMajor = (major + 1) / 2; // [0, 1] but is more of a normal distribution
      let normalMajor2 = (major2 + 1) / 2; // [0, 1] but is more of a normal distribution

      minorNoise[i] = 1 - normalMinor;
      majorNoise[i] = 1 - normalMajor;
      majorNoise2[i] = 1 - normalMajor2;

      let z = Math.max(-1, Math.min(1, major * 3));

      z = Math.min(z, (major2 + 1) * 3 - 1);

      let roughness = 1 - (1 - normalMinor) ** 4;
      z = (z * roughness + 1) / 2;

      z = altitudeApproximation(z);
      // z = z * 2 - 1
      // z *= 5

      // let z = major;

      positionBuffer[i * 3 + 1] = z;

      height[i] = z;
    }

    const minElevation = height.reduce(
      (prev, curr) => (prev < curr ? prev : curr),
      Infinity,
    );
    const maxElevation = height.reduce(
      (prev, curr) => (prev > curr ? prev : curr),
      -Infinity,
    );

    height.forEach(
      (val, index, array) =>
        (array[index] = (val - minElevation) / (maxElevation - minElevation)),
    ); // Normalize

    return {
      positionBuffer,
      minorNoise,
      majorNoise,
      majorNoise2,
      height,
    };
  };
}
