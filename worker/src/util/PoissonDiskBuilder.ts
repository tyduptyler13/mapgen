import { PRNG, Seed } from "./random";
import RandomAccessMap from "./collections/RandomAccessMap";
import MapData from "./MapData";

export interface Point {
  x: number;
  y: number;
}

export interface PoissonDiskBuilderFlags {
  /**
   * Center the points around zero, otherwise points will go from 0-maxWidth/height for each respective dimension
   */
  center: boolean;

  /**
   * How many times to try to insert a point before greedily giving up (poisson disk)
   *
   * Note: Changing this could result in different output so treat it as part of the seed
   *
   * Defaults to 30
   */
  maxAttempts: number;
}

// const MIN_DISTANCE = 1 / Math.sqrt(2);
// We can't change this because this implementation has optimizations expecting the grid size is "normal" intervals of 1 unit
const MIN_DISTANCE_SQRD = 0.5;
const MAX_ATTEMPTS = 30;

export default function applyPoissonDisk(
  map: MapData,
  {
    seed = 0,
    center = true,
    maxAttempts = MAX_ATTEMPTS,
  }: {
    seed: Seed;
  } & Partial<PoissonDiskBuilderFlags> = {
    seed: 0,
    center: true,
    maxAttempts: MAX_ATTEMPTS,
  },
) {
  const activeCache = new RandomAccessMap<number, Point>(); // We use a map because sets don't evaluate deep equality
  const prng: PRNG = new PRNG(seed);

  const poissonDiskFlags: PoissonDiskBuilderFlags = {
    center,
    maxAttempts,
  };

  // Bindings
  const toOffset = map.toOffset.bind(map);
  const has = map.has.bind(map);
  const get = map.get.bind(map);
  const set = map.set.bind(map);
  const maxWidth = map.maxWidth;
  const maxHeight = map.maxHeight;

  function updateActivePoints(gridCell: Point) {
    // Walks around the current grid cell
    for (let dx = -1; dx <= 1; ++dx) {
      for (let dy = -1; dy <= 1; ++dy) {
        if (dx === 0 && dy === 0) {
          continue;
        }

        const possibleInactivePoint = {
          x: (gridCell.x + dx + maxWidth) % maxWidth,
          y: (gridCell.y + dy + maxHeight) % maxHeight,
        };

        let offset = toOffset(possibleInactivePoint);
        if (!activeCache.has(offset) && !has(offset)) {
          // This point is inactive and missing a point, so add it
          activeCache.set(offset, possibleInactivePoint);
        }
      }
    }
  }

  function tryInsertPoint(gridCell: Point): boolean {
    const possiblePosition = {
      x: gridCell.x + prng.get(),
      y: gridCell.y + prng.get(),
    };

    for (let dx = -1; dx <= 1; ++dx) {
      for (let dy = -1; dy <= 1; ++dy) {
        if (dx === 0 && dy === 0) {
          continue;
        }

        const possibleCompareCell = { x: gridCell.x + dx, y: gridCell.y + dy };

        // Handle special edge case where we loop the points for best possible fit on edges
        if (
          possibleCompareCell.x >= maxWidth ||
          possibleCompareCell.x < 0 ||
          possibleCompareCell.y >= maxHeight ||
          possibleCompareCell.y < 0
        ) {
          // Circular number ranges are easy to manipulate
          const x = (possibleCompareCell.x + maxWidth) % maxWidth;
          const y = (possibleCompareCell.y + maxHeight) % maxHeight;
          let point = { x, y };
          const offset = toOffset(point);
          if (has(offset)) {
            const value = get(point);
            // Adjust based on original offset
            value.x =
              possibleCompareCell.x < 0
                ? value.x - maxWidth
                : possibleCompareCell.x >= maxWidth
                ? value.x + maxWidth
                : value.x;
            value.y =
              possibleCompareCell.y < 0
                ? value.y - maxHeight
                : possibleCompareCell.y >= maxHeight
                ? value.y + maxHeight
                : value.y;
            const distanceToSquared =
              (possiblePosition.x - value.x) ** 2 +
              (possiblePosition.y - value.y) ** 2;
            if (distanceToSquared < MIN_DISTANCE_SQRD) {
              return false;
            }
          }
          continue;
        }

        // Simple cases
        const offset = toOffset(possibleCompareCell);
        if (has(offset)) {
          const value = get(possibleCompareCell);
          const distanceToSquared =
            (possiblePosition.x - value.x) ** 2 +
            (possiblePosition.y - value.y) ** 2;
          if (distanceToSquared < MIN_DISTANCE_SQRD) {
            return false;
          }
        }
      }
    }

    const offset = set(gridCell, possiblePosition);
    updateActivePoints(gridCell);
    activeCache.delete(offset);

    return true;
  }

  const startingPoint = {
    x: prng.randomInRange(maxWidth),
    y: prng.randomInRange(maxHeight),
  };
  const startingGridCell = {
    x: Math.floor(startingPoint.x),
    y: Math.floor(startingPoint.y),
  };
  set(startingGridCell, startingPoint);

  updateActivePoints(startingGridCell);

  while (activeCache.size > 0) {
    const currentCell = activeCache.getRandom(prng);
    let success = false;
    for (let i = 0; i < poissonDiskFlags.maxAttempts; ++i) {
      if (tryInsertPoint(currentCell)) {
        success = true;
        break;
      }
    }
    if (!success) {
      // This point failed too many times, just remove it
      activeCache.delete(toOffset(currentCell));
    }
  }

  if (poissonDiskFlags.center) {
    for (let i = 0; i < map.points.length / 2; ++i) {
      map.points[i * 2] -= maxWidth / 2;
      map.points[i * 2 + 1] -= maxHeight / 2;
    }
  }

  return map;
}
