import { MersenneTwister } from "./MersenneTwister";

export type Seed = number | number[];

export class PRNG {
  private mersenneTwister;

  constructor(seed: Seed = 1234567) {
    this.mersenneTwister = new MersenneTwister(seed);
  }

  get(): number {
    return this.mersenneTwister.random();
  }

  randomInRange(max: number, min: number = 0): number {
    return this.get() * (max - min) + min;
  }

  randomIntInRange(max: number, min: number = 0): number {
    return Math.floor(this.randomInRange(max, min));
  }
}
