import { Point } from "./PoissonDiskBuilder";

export interface Node {
  position: Point;
  /**
   * A list of indices that this node is connected to
   */
  edges: number[];
}

export interface IndexedPoint {
  i: number;
  p: Point;
}

export interface IMapData {
  /*
   * Insertion order pairs of points
   */
  points: Float32Array;

  /*
   * Row major indices of points (the index needs to be multiplied by 2 to get the actual point since the points array is pairs (x,y))
   */
  indexArray: Int32Array;

  /**
   * Triples of vec3 forming triangles (should be at most ({# of points} - 1) * 3 elements)
   *
   * Only present after triangulation
   */
  triangles?: Float32Array;

  /**
   * A graph of all the points and which points are reachable from that point, only present if this map has been triangulated
   */
  dag?: Node[];
}

export default class MapData implements IMapData {
  indexArray: Int32Array;
  points: Float32Array;

  dag?: Node[];
  triangles?: Float32Array;
  horizontalStitch?: Float32Array;
  verticalStitch?: Float32Array;
  holes?: Float32Array;

  private currentIndex: number = 0;

  constructor(
    readonly maxWidth: number,
    readonly maxHeight: number,
  ) {
    this.points = new Float32Array(maxWidth * maxHeight * 2);
    this.indexArray = new Int32Array(maxWidth * maxHeight);
    this.indexArray.fill(-1); // Need to distinguish from missing values and index 0
  }

  set(point: Point, value: Point): number {
    const offset = this.toOffset(point);
    this.points[this.currentIndex * 2] = value.x;
    this.points[this.currentIndex * 2 + 1] = value.y;
    console.assert(this.indexArray[offset] === -1); // Avoid writing twice
    this.indexArray[offset] = this.currentIndex++;
    return offset;
  }

  toOffset(point: Point): number {
    return point.x + point.y * this.maxWidth;
  }

  fromOffset(offset: number): Point {
    const x = offset % this.maxWidth;
    return {
      x,
      y: (offset - x) / this.maxWidth,
    };
  }

  get(point: Point): Point {
    const offset = this.toOffset(point);
    return {
      x: this.points[this.indexArray[offset] * 2],
      y: this.points[this.indexArray[offset] * 2 + 1],
    };
  }

  /*
   * This requires the offset because I never found the case where the offset wasn't required again as a key for the active set,
   * recalculating it when we already have the offset is a waste.
   */
  has(offset: number): boolean {
    return this.indexArray[offset] !== -1;
  }

  /**
   * Truncates the points array to make it more compact
   */
  truncate() {
    this.points = this.points.slice(0, this.currentIndex * 2);
  }

  getOrPutDag(index: number, put: () => Node): Node {
    if (!this.dag) {
      this.dag = [];
    }

    if (this.dag.length <= index) {
      this.dag.length = this.indexArray.length;
    }

    if (!this.dag[index]) {
      this.dag[index] = put();
    }

    return this.dag[index];
  }

  private static pushUniqueToNode(node: Node, ...elements: number[]) {
    for (let i = 0; i < elements.length; i++) {
      if (node.edges.indexOf(elements[i]) === -1) {
        node.edges.push(elements[i]);
      }
    }
  }

  connectPoints(A: IndexedPoint, B: IndexedPoint, C: IndexedPoint) {
    MapData.pushUniqueToNode(
      this.getOrPutDag(A.i, () => ({ edges: [], position: A.p })),
      B.i,
      C.i,
    );
    MapData.pushUniqueToNode(
      this.getOrPutDag(B.i, () => ({ edges: [], position: B.p })),
      A.i,
      C.i,
    );
    MapData.pushUniqueToNode(
      this.getOrPutDag(C.i, () => ({ edges: [], position: C.p })),
      A.i,
      B.i,
    );
  }

  getNode(point: { x: number; y: number }) {
    return this.dag?.[this.indexArray[this.toOffset(point)]];
  }

  getIndex(point: { x: number; y: number }) {
    return this.indexArray[this.toOffset(point)];
  }
}
