import { inCircle, isCcw } from "./math/Triangle";
import MapData, { IndexedPoint } from "./MapData";
import { Point } from "./PoissonDiskBuilder";

enum DIRECTION {
  N,
  W,
  E,
  S,
  NW,
  NE,
  SW,
  SE,
}

const DIRECTION_OFFSETS: readonly number[][] = [
  [0, 1],
  [-1, 0],
  [1, 0],
  [0, -1],
  [-1, 1],
  [1, 1],
  [-1, -1],
  [1, -1],
];

interface TriangulationPoints {
  Agp: Point;
  Bgp: Point;
  Cgp: Point;
  Dgp: Point;
}

export default function triangulate(map: MapData) {
  const toOffset = map.toOffset.bind(map);
  const fromOffset = map.fromOffset.bind(map);
  const connectPoints = map.connectPoints.bind(map);
  const indexArray = map.indexArray;
  const points = map.points;

  function triangulateQuad(
    triangleBuffer: number[],
    A: IndexedPoint,
    B: IndexedPoint,
    C: IndexedPoint,
    D: IndexedPoint,
  ) {
    const flipCcw =
      isCcw(A.p.x, A.p.y, C.p.x, C.p.y, B.p.x, B.p.y) &&
      isCcw(B.p.x, B.p.y, C.p.x, C.p.y, D.p.x, D.p.y);
    const flopCcw =
      isCcw(A.p.x, A.p.y, C.p.x, C.p.y, D.p.x, D.p.y) &&
      isCcw(A.p.x, A.p.y, D.p.x, D.p.y, B.p.x, B.p.y);
    const flipDInCircle = inCircle(
      A.p.x,
      A.p.y,
      B.p.x,
      B.p.y,
      C.p.x,
      C.p.y,
      D.p.x,
      D.p.y,
    );
    const flopBInCircle = inCircle(
      A.p.x,
      A.p.y,
      C.p.x,
      C.p.y,
      D.p.x,
      D.p.y,
      B.p.x,
      B.p.y,
    );

    // Pick flip or flow dependending on if the triangles generated would be considered degenerate. If D would endup inside the triangle ABC,
    // then choose flip, if B would end up in the triangle ACD then choose flop. If neither triangle is degenerate, choose based on if the triangles of flip
    // are in an order to satisfy the counter clockwise requirement of flip.
    if ((!flipDInCircle && flipCcw) || flopBInCircle) {
      /*
					(flip)
				 A ---- B
				 |    / |
				 |   /  |
				 |  /   |
				 | /    |
				 |/     |
				 C------D

				 Triangles need to be counterclockwise, so the order will be (A C B) and (B C D) [flip]
			*/

      // left
      triangleBuffer.push(A.p.x, 0, A.p.y, C.p.x, 0, C.p.y, B.p.x, 0, B.p.y);

      // Right
      triangleBuffer.push(B.p.x, 0, B.p.y, C.p.x, 0, C.p.y, D.p.x, 0, D.p.y);

      connectPoints(A, B, C);
      connectPoints(B, C, D);
    } else {
      // flop

      /*
				(flop)
				A --- B
				|\    |
				| \   |
				|  \  |
				|   \ |
				|    \|
				C --- D

				(A C D) and (A D B) if the line needs to be flipped [flop]
			 */

      // left
      triangleBuffer.push(A.p.x, 0, A.p.y, C.p.x, 0, C.p.y, D.p.x, 0, D.p.y);

      // Right
      triangleBuffer.push(A.p.x, 0, A.p.y, D.p.x, 0, D.p.y, B.p.x, 0, B.p.y);

      connectPoints(A, C, D);
      connectPoints(A, B, D);
    }
  }

  function triangulate(
    holes: Set<number>,
    pointsGenerator: Generator<TriangulationPoints>,
  ): Float32Array {
    const triangleBuffer: number[] = [];

    let current: IteratorResult<TriangulationPoints> = pointsGenerator.next();

    // Vertical index is a paired lookahead
    while (!current.done) {
      const { Agp, Bgp, Cgp, Dgp } = current.value;

      current = pointsGenerator.next();

      const Ai = indexArray[toOffset(Agp)];
      const Bi = indexArray[toOffset(Bgp)];
      const Ci = indexArray[toOffset(Cgp)];
      const Di = indexArray[toOffset(Dgp)];

      let skip = false;
      if (Bi === -1) {
        skip = true;
        holes.add(toOffset(Bgp));
      }

      if (Di === -1) {
        skip = true;
        holes.add(toOffset(Dgp));
      }

      // Previous hole
      if (Ai === -1) {
        skip = true;
        holes.add(toOffset(Agp));
      }

      if (Ci === -1) {
        skip = true;
        holes.add(toOffset(Cgp));
      }

      if (skip) {
        continue;
      }

      const A: IndexedPoint = {
        p: {
          x: points[Ai * 2],
          y: points[Ai * 2 + 1],
        },
        i: Ai,
      };
      const B: IndexedPoint = {
        p: {
          x: points[Bi * 2],
          y: points[Bi * 2 + 1],
        },
        i: Bi,
      };
      const C: IndexedPoint = {
        p: {
          x: points[Ci * 2],
          y: points[Ci * 2 + 1],
        },
        i: Ci,
      };
      const D: IndexedPoint = {
        p: {
          x: points[Di * 2],
          y: points[Di * 2 + 1],
        },
        i: Di,
      };

      triangulateQuad(triangleBuffer, A, B, C, D);
    }

    return new Float32Array(triangleBuffer);
  }

  function holeFill(holes: Set<number>) {
    // Keyhole hole filler
    const triangleBuffer: number[] = [];

    function createTriangle(A: IndexedPoint, D: IndexedPoint, B: IndexedPoint) {
      triangleBuffer.push(A.p.x, 0, A.p.y, D.p.x, 0, D.p.y, B.p.x, 0, B.p.y);
      connectPoints(A, D, B);
    }

    // Simple holes (single hole with no neighbor holes)
    Array.from(holes).forEach((offset) => {
      const position = fromOffset(offset);

      const wrapDirections = {
        [DIRECTION.N]: position.y === map.maxHeight - 1,
        [DIRECTION.W]: position.x === 0,
        [DIRECTION.E]: position.x === map.maxWidth - 1,
        [DIRECTION.S]: position.y === 0,
      };

      const skip: Record<DIRECTION, boolean> = {
        [DIRECTION.N]: false,
        [DIRECTION.W]: false,
        [DIRECTION.E]: false,
        [DIRECTION.S]: false,
        [DIRECTION.NW]: false,
        [DIRECTION.NE]: false,
        [DIRECTION.SW]: false,
        [DIRECTION.SE]: false,
      };

      // Check if we have more than one hole next to eachother, this is extremely rare but changes handling (2, and 3 have been observed)
      for (let i = 0; i < DIRECTION_OFFSETS.length; ++i) {
        const [dx, dy] = DIRECTION_OFFSETS[i];
        if (holes.has(toOffset({ x: position.x + dx, y: position.y + dy }))) {
          skip[i as DIRECTION] = true;
        }
      }

      /*
				 ABC
				 D E
				 FGH
			 */
      const xMax = wrapDirections[DIRECTION.E] ? 0 : position.x + 1;
      const yMax = wrapDirections[DIRECTION.N] ? 0 : position.y + 1;
      const xMin = wrapDirections[DIRECTION.W]
        ? map.maxWidth - 1
        : position.x - 1;
      const yMin = wrapDirections[DIRECTION.S]
        ? map.maxHeight - 1
        : position.y - 1;

      const xMaxCorrection = wrapDirections[DIRECTION.E] ? map.maxWidth : 0;
      const yMaxCorrection = wrapDirections[DIRECTION.N] ? map.maxHeight : 0;
      const xMinCorrection = wrapDirections[DIRECTION.W] ? -map.maxWidth : 0;
      const yMinCorrection = wrapDirections[DIRECTION.S] ? -map.maxHeight : 0;

      const Agp = { x: xMin, y: yMax };
      const Bgp = { x: position.x, y: yMax };
      const Cgp = { x: xMax, y: yMax };
      const Dgp = { x: xMin, y: position.y };
      const Egp = { x: xMax, y: position.y };
      const Fgp = { x: xMin, y: yMin };
      const Ggp = { x: position.x, y: yMin };
      const Hgp = { x: xMax, y: yMin };

      const Ai = indexArray[toOffset(Agp)];
      const Bi = indexArray[toOffset(Bgp)];
      const Ci = indexArray[toOffset(Cgp)];
      const Di = indexArray[toOffset(Dgp)];
      const Ei = indexArray[toOffset(Egp)];
      const Fi = indexArray[toOffset(Fgp)];
      const Gi = indexArray[toOffset(Ggp)];
      const Hi = indexArray[toOffset(Hgp)];

      const A: IndexedPoint = {
        p: {
          x: points[Ai * 2] + xMinCorrection,
          y: points[Ai * 2 + 1] + yMaxCorrection,
        },
        i: Ai,
      };
      const B: IndexedPoint = {
        p: {
          x: points[Bi * 2],
          y: points[Bi * 2 + 1] + yMaxCorrection,
        },
        i: Bi,
      };
      const C: IndexedPoint = {
        p: {
          x: points[Ci * 2] + xMaxCorrection,
          y: points[Ci * 2 + 1] + yMaxCorrection,
        },
        i: Ci,
      };
      const D: IndexedPoint = {
        p: {
          x: points[Di * 2] + xMinCorrection,
          y: points[Di * 2 + 1],
        },
        i: Di,
      };
      const E: IndexedPoint = {
        p: {
          x: points[Ei * 2] + xMaxCorrection,
          y: points[Ei * 2 + 1],
        },
        i: Ei,
      };
      const F: IndexedPoint = {
        p: {
          x: points[Fi * 2] + xMinCorrection,
          y: points[Fi * 2 + 1] + yMinCorrection,
        },
        i: Fi,
      };
      const G: IndexedPoint = {
        p: {
          x: points[Gi * 2],
          y: points[Gi * 2 + 1] + yMinCorrection,
        },
        i: Gi,
      };
      const H: IndexedPoint = {
        p: {
          x: points[Hi * 2] + xMaxCorrection,
          y: points[Hi * 2 + 1] + yMinCorrection,
        },
        i: Hi,
      };

      // Fill the corners
      if (!(skip[DIRECTION.NW] || skip[DIRECTION.N] || skip[DIRECTION.W])) {
        createTriangle(A, D, B);
      }
      if (!(skip[DIRECTION.NE] || skip[DIRECTION.N] || skip[DIRECTION.E])) {
        createTriangle(B, E, C);
      }
      if (!(skip[DIRECTION.SW] || skip[DIRECTION.S] || skip[DIRECTION.W])) {
        createTriangle(D, F, G);
      }
      if (!(skip[DIRECTION.SE] || skip[DIRECTION.S] || skip[DIRECTION.E])) {
        createTriangle(E, G, H);
      }

      /*
				 -B-
				 D E
				 -G-
			 */

      // Fill the center
      if (
        !(
          skip[DIRECTION.N] ||
          skip[DIRECTION.W] ||
          skip[DIRECTION.E] ||
          skip[DIRECTION.S]
        )
      ) {
        // Regular case
        triangulateQuad(triangleBuffer, B, E, D, G);
      } else {
        // Handle edge cases

        // Special cases
        if (!(skip[DIRECTION.W] || skip[DIRECTION.E] || skip[DIRECTION.S])) {
          // Bottom only
          createTriangle(D, G, E);
        } else if (
          !(skip[DIRECTION.N] || skip[DIRECTION.E] || skip[DIRECTION.S])
        ) {
          // Right only
          createTriangle(B, G, E);
        } else if (
          !(skip[DIRECTION.N] || skip[DIRECTION.W] || skip[DIRECTION.S])
        ) {
          // Left only
          createTriangle(B, D, G);
        } else if (
          !(skip[DIRECTION.N] || skip[DIRECTION.W] || skip[DIRECTION.E])
        ) {
          // Top only
          createTriangle(B, D, E);
        }

        if (
          skip[DIRECTION.N] &&
          !(
            skip[DIRECTION.NW] ||
            skip[DIRECTION.NE] ||
            skip[DIRECTION.W] ||
            skip[DIRECTION.E]
          )
        ) {
          // We can also build the triangle to fill the gap and assume it gets filled too

          /* ???
						 AxC
						 DxE
						 ---
					 */
          triangulateQuad(triangleBuffer, A, C, D, E);
        }

        if (
          skip[DIRECTION.E] &&
          !(
            skip[DIRECTION.NE] ||
            skip[DIRECTION.N] ||
            skip[DIRECTION.S] ||
            skip[DIRECTION.SE]
          )
        ) {
          // We can also build the triangle to fill the gap and assume it gets filled too

          /*
						 -BC?
						 -xx?
						 -GH?
					 */
          triangulateQuad(triangleBuffer, B, C, G, H);
        }

        // TODO solve L shaped holes (rare)

        // We don't need the other directions, otherwise we'll have overlap
      }
    });

    return new Float32Array(triangleBuffer);
  }

  function holes2Points(holes: Iterable<number>) {
    return new Float32Array(
      Array.from(holes).reduce((prev, it) => {
        const point = fromOffset(it);
        prev.push(point.x, 0, point.y);
        return prev;
      }, [] as number[]),
    );
  }

  function triangulatePrimary(): { fill: Float32Array; holes: Set<number> } {
    // return triangulateRow(0, 1);
    const rows: Array<Float32Array> = [];
    const holes = new Set<number>();

    for (let i = 0; i < map.maxHeight - 1; ++i) {
      rows.push(
        triangulate(holes, horizontalGeneratorFactory(i, i + 1, map.maxWidth)),
      );
    }

    const array = new Float32Array(
      rows.reduce((prev, curr) => prev + curr.length, 0),
    );

    let offset = 0;
    rows.forEach((row) => {
      array.set(row, offset);
      offset += row.length;
    });

    return {
      fill: array,
      holes: holes,
    };
  }

  function triangulateHorizontalStitch(): {
    stitch: Float32Array;
    holes: Set<number>;
  } {
    const holes = new Set<number>();

    const stitch = triangulate(
      holes,
      horizontalGeneratorFactory(0, map.maxHeight - 1, map.maxWidth),
    );

    for (let i = 2; i < stitch.length; i += 3) {
      if (stitch[i] < 0) {
        stitch[i] += map.maxHeight;
      }
    }

    return {
      stitch,
      holes,
    };
  }

  function triangulateVerticalStitch(): {
    stitch: Float32Array;
    holes: Set<number>;
  } {
    const holes = new Set<number>();

    const stitch = triangulate(
      holes,
      verticalGeneratorFactory(map.maxWidth - 1, 0, map.maxHeight),
    );

    for (let i = 0; i < stitch.length; i += 3) {
      if (stitch[i] < 0) {
        stitch[i] += map.maxWidth;
      }
    }

    return {
      stitch,
      holes,
    };
  }

  function triangulateCorner(): number[] {
    const Agp = { x: map.maxWidth - 1, y: 0 };
    const Bgp = { x: 0, y: 0 };
    const Cgp = { x: map.maxWidth - 1, y: map.maxHeight - 1 };
    const Dgp = { x: 0, y: map.maxHeight - 1 };
    const Ai = indexArray[toOffset(Agp)];
    const Bi = indexArray[toOffset(Bgp)];
    const Ci = indexArray[toOffset(Cgp)];
    const Di = indexArray[toOffset(Dgp)];

    const A: IndexedPoint = {
      p: {
        x: points[Ai * 2],
        y: points[Ai * 2 + 1] + map.maxHeight,
      },
      i: Ai,
    };
    const B: IndexedPoint = {
      p: {
        x: points[Bi * 2] + map.maxWidth,
        y: points[Bi * 2 + 1] + map.maxHeight,
      },
      i: Bi,
    };
    const C: IndexedPoint = {
      p: {
        x: points[Ci * 2],
        y: points[Ci * 2 + 1],
      },
      i: Ci,
    };
    const D: IndexedPoint = {
      p: {
        x: points[Di * 2] + map.maxWidth,
        y: points[Di * 2 + 1],
      },
      i: Di,
    };

    const triangles: number[] = [];

    triangulateQuad(triangles, A, B, C, D);

    return triangles;
  }

  const mainResult = triangulatePrimary();
  const horizontalResult = triangulateHorizontalStitch();
  const verticalResult = triangulateVerticalStitch();
  const cornerResult = triangulateCorner();

  const holes = new Set([
    ...mainResult.holes,
    ...horizontalResult.holes,
    ...verticalResult.holes,
  ]);

  const holesTriangles = holeFill(holes);

  const toMergeTriangles = [
    mainResult.fill,
    holesTriangles,
    horizontalResult.stitch,
    verticalResult.stitch,
    cornerResult,
  ];

  const allTriangles = new Float32Array(
    toMergeTriangles.reduce((prev, curr) => prev + curr.length, 0),
  );

  let offset = 0;
  toMergeTriangles.forEach((it) => {
    allTriangles.set(it, offset);
    offset += it.length;
  });

  map.triangles = allTriangles;
  map.horizontalStitch = horizontalResult.stitch;
  map.verticalStitch = verticalResult.stitch;
  map.holes = holes2Points(holes);

  return map;
}

function* horizontalGeneratorFactory(
  topRow: number,
  bottomRow: number,
  count: number,
): Generator<TriangulationPoints> {
  for (let x = 0; x < count - 1; ++x) {
    yield {
      Agp: { x, y: topRow },
      Bgp: { x: x + 1, y: topRow },
      Cgp: { x, y: bottomRow },
      Dgp: { x: x + 1, y: bottomRow },
    };
  }

  const x = count - 1;
  return {
    Agp: { x, y: topRow },
    Bgp: { x: x + 1, y: topRow },
    Cgp: { x, y: bottomRow },
    Dgp: { x: x + 1, y: bottomRow },
  };
}

function* verticalGeneratorFactory(
  leftRow: number,
  rightRow: number,
  count: number,
): Generator<TriangulationPoints> {
  for (let y = 0; y < count - 1; ++y) {
    yield {
      Agp: { x: leftRow, y },
      Bgp: { x: leftRow, y: y + 1 },
      Cgp: { x: rightRow, y },
      Dgp: { x: rightRow, y: y + 1 },
    };
  }

  const y = count - 1;
  return {
    Agp: { x: leftRow, y },
    Bgp: { x: leftRow, y: y + 1 },
    Cgp: { x: rightRow, y },
    Dgp: { x: rightRow, y: y + 1 },
  };
}
