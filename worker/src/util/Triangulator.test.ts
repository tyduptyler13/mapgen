import MapData from "./MapData";
import triangulate from "./Triangulator";
import { Point } from "./PoissonDiskBuilder";

describe("Triangulator", () => {
  describe("Edge check", () => {
    it("Fills top gap", () => {
      let hole = { x: 1, y: 2 };
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes: [hole] }),
      );

      expect(data.getNode({ x: 0, y: 2 })?.edges).toContain(
        data.getIndex({ x: 2, y: 2 }),
      );
    });

    it("Fills left gap", () => {
      let hole = { x: 0, y: 1 };
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes: [hole] }),
      );

      expect(data.getNode({ x: 0, y: 0 })?.edges).toContain(
        data.getIndex({ x: 0, y: 2 }),
      );
    });

    it("Fills right gap", () => {
      let hole = { x: 2, y: 1 };
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes: [hole] }),
      );

      expect(data.getNode({ x: 2, y: 0 })?.edges).toContain(
        data.getIndex({ x: 2, y: 2 }),
      );
    });

    it("Fills bottom gap", () => {
      let hole = { x: 1, y: 0 };
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes: [hole] }),
      );

      expect(data.getNode({ x: 0, y: 0 })?.edges).toContain(
        data.getIndex({ x: 2, y: 0 }),
      );
    });
  });

  describe("Double hole check", () => {
    it("Bottom double hole", () => {
      let holes = [
        { x: 1, y: 0 },
        { x: 1, y: 1 },
      ];
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes }),
      );

      expect(data.getNode({ x: 0, y: 0 })?.edges).toContain(
        data.getIndex({ x: 2, y: 0 }),
      );
    });

    it("Left double hole", () => {
      let holes = [
        { x: 0, y: 1 },
        { x: 1, y: 1 },
      ];
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes }),
      );

      expect(data.getNode({ x: 0, y: 0 })?.edges).toContain(
        data.getIndex({ x: 0, y: 2 }),
      );
    });

    it("Right double hole", () => {
      let holes = [
        { x: 1, y: 1 },
        { x: 2, y: 1 },
      ];
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes }),
      );

      expect(data.getNode({ x: 2, y: 2 })?.edges).toContain(
        data.getIndex({ x: 2, y: 0 }),
      );
    });

    it("Top double hole", () => {
      let holes = [
        { x: 1, y: 1 },
        { x: 1, y: 2 },
      ];
      const data = triangulate(
        gapBuilder({ dimensions: { x: 3, y: 3 }, holes }),
      );

      expect(data.getNode({ x: 0, y: 2 })?.edges).toContain(
        data.getIndex({ x: 2, y: 2 }),
      );
    });
  });

  describe("triple L shaped hole check", () => {
    // TODO NE, NW, SE, SW
  });
});

function gapBuilder({
  dimensions: { x, y },
  holes = [],
}: {
  holes?: Point[];
  dimensions: Point;
}): MapData {
  const data = new MapData(x, y);

  const holesSet = new Set(holes.map((it) => data.toOffset(it)));

  for (let j = 0; j < y; ++j) {
    for (let i = 0; i < x; ++i) {
      const p = { x: i, y: j };
      if (holesSet.has(data.toOffset(p))) {
        continue;
      }

      data.set(p, p);
    }
  }

  return data;
}
