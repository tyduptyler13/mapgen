import { PRNG } from "../random";

export default class RandomAccessMap<K, T> {
  private keys: Array<K> = [];
  private keyIndexCache = new Map<K, number>();
  private map = new Map<K, T>();

  get size() {
    return this.keys.length;
  }

  has(key: K): boolean {
    return this.map.has(key);
  }

  set(key: K, value: T) {
    this.map.set(key, value);
    this.keyIndexCache.set(key, this.keys.push(key) - 1);
  }

  delete(key: K) {
    const index = this.keyIndexCache.get(key);
    if (index === undefined || this.keys.length === 0) {
      return;
    }

    const lastKey = this.keys.at(-1)!;
    if (lastKey === key) {
      this.keys.pop();
    } else {
      this.keys[index] = lastKey;
      this.keys.pop();
      this.keyIndexCache.set(lastKey, index);
    }

    this.keyIndexCache.delete(key);
    this.map.delete(key);
  }

  getRandom(prng: PRNG): T {
    return this.map.get(this.keys[prng.randomIntInRange(this.size)])!;
  }
}
