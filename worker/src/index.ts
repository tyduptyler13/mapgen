import {
  isGenerateMapRequest,
  isRegenerateAltitudeRequest,
  TriangulationResponse,
  WorkerMessage,
} from "./types";
import { makeLoopingAltitudeGenerator } from "./util/landSampler";
import MapData from "./util/MapData";
import applyPoissonDisk from "./util/PoissonDiskBuilder";
import triangulate from "./util/Triangulator";

function sendTriangulation(result: Omit<TriangulationResponse, "messageType">) {
  const response: TriangulationResponse = {
    messageType: WorkerMessage.TriangulationResponse,
    ...result,
  };
  postMessage(response, [
    result.points.buffer,
    result.positionBuffer.buffer,
    result.minorNoise.buffer,
    result.majorNoise.buffer,
    result.majorNoise2.buffer,
    result.height.buffer,
    result.holes.buffer,
  ]);
}

let map: MapData;

addEventListener("message", ({ data }) => {
  if (isGenerateMapRequest(data)) {
    const { maxWidth, maxHeight, seed, scales } = data;
    map = new MapData(maxWidth, maxHeight);
    applyPoissonDisk(map, {
      seed: seed ?? 0,
    });
    triangulate(map);

    const altitudeGenerator = makeLoopingAltitudeGenerator(seed);
    const result = altitudeGenerator(
      map.triangles!,
      maxWidth,
      maxHeight,
      scales,
    );
    sendTriangulation({
      ...result,
      // Copy these so our local variable isn't suddenly empty
      points: new Float32Array(map.points),
      positionBuffer: new Float32Array(map.triangles!),
      holes: new Float32Array(map.holes!),
      verticalStitch: new Float32Array(map.verticalStitch!),
      horizontalStitch: new Float32Array(map.horizontalStitch!),
    });
  } else if (isRegenerateAltitudeRequest(data)) {
    const { maxWidth, maxHeight, seed, scales } = data;

    const altitudeGenerator = makeLoopingAltitudeGenerator(seed);
    const result = altitudeGenerator(
      map.triangles!,
      maxWidth,
      maxHeight,
      scales,
    );
    sendTriangulation({
      ...result,
      // Copy these so our local variable isn't suddenly empty
      points: new Float32Array(map.points),
      positionBuffer: new Float32Array(map.triangles!),
      holes: new Float32Array(map.holes!),
      verticalStitch: new Float32Array(map.verticalStitch!),
      horizontalStitch: new Float32Array(map.horizontalStitch!),
    });
  } else {
    console.error("Unknown message", data);
  }
});
