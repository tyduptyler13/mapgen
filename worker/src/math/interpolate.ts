import { inv, matrix, multiply, transpose } from "mathjs";

export function interpolate(
  points: Array<[number, number]>,
  degree: number = points.length - 1,
): (x: number) => number {
  const X = matrix(
    points.map(([x, _]) =>
      Array.from({ length: degree + 1 }, (_, m) => x ** m),
    ),
  );
  const Xt = transpose(X);
  const invXtX = inv(multiply(Xt, X));
  const Y = matrix(points.map(([_, y]) => [y]));

  const rawCoefficients = multiply(multiply(invXtX, Xt), Y);
  const coefficients = rawCoefficients
    .valueOf()
    .flat()
    .map((c) => {
      if (typeof c === "number") {
        return c;
      } else {
        throw new Error("interpolate resulted in unsupported number type");
      }
    });

  return function evaluate(x: number): number {
    let sum = 0;
    for (let i = 0; i <= degree; ++i) {
      sum += coefficients[i] * x ** i;
    }
    return sum;
  };
}
