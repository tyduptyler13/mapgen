import { Seed } from "./util/random";

export enum WorkerMessage {
  GenerateMapRequest,
  TriangulationResponse,
  RegenerateAltitudeRequest,
}

export interface AltitudeScales {
  minorScale?: number;
  majorScale?: number;
  distortionScale?: number;
}

export interface GenerateMapRequest {
  messageType: WorkerMessage.GenerateMapRequest;
  maxWidth: number;
  maxHeight: number;
  scales?: AltitudeScales;
  seed?: Seed;
}

export function isGenerateMapRequest(obj: unknown): obj is GenerateMapRequest {
  return (
    !!obj &&
    typeof obj === "object" &&
    "messageType" in obj &&
    obj.messageType === WorkerMessage.GenerateMapRequest
  );
}

export interface TriangulationResponse {
  messageType: WorkerMessage.TriangulationResponse;
  positionBuffer: Float32Array;
  verticalStitch: Float32Array;
  horizontalStitch: Float32Array;
  majorNoise: Float32Array;
  majorNoise2: Float32Array;
  minorNoise: Float32Array;
  points: Float32Array;
  height: Float32Array;
  holes: Float32Array;
}

export function isTriangulationResponse(
  obj: unknown,
): obj is TriangulationResponse {
  return (
    !!obj &&
    typeof obj === "object" &&
    "messageType" in obj &&
    obj.messageType === WorkerMessage.TriangulationResponse
  );
}

export interface RegenerateAltitudeRequest {
  messageType: WorkerMessage.RegenerateAltitudeRequest;
  maxWidth: number;
  maxHeight: number;
  scales: AltitudeScales;
  seed?: Seed;
}

export function isRegenerateAltitudeRequest(
  obj: unknown,
): obj is RegenerateAltitudeRequest {
  return (
    !!obj &&
    typeof obj === "object" &&
    "messageType" in obj &&
    obj.messageType === WorkerMessage.RegenerateAltitudeRequest
  );
}
