import { BufferAttribute, BufferGeometry } from "three";

export function applyBarycentricAttribute(
  bufferGeometry: BufferGeometry,
  removeEdge = false,
) {
  if (bufferGeometry.getIndex()) {
    // We must de-index, lets use the official method instead of the one from the provided code
    bufferGeometry.copy(bufferGeometry.toNonIndexed());
  }
  // See https://github.com/mattdesl/webgl-wireframes/blob/bf073f8f18215dec87cb910f6da3775f1e8bfb6f/lib/geom.js
  const attrib =
    bufferGeometry.getIndex() || bufferGeometry.getAttribute("position");
  const count = attrib.count / 3;
  const barycentric = [];

  // for each triangle in the geometry, add the barycentric coordinates
  for (let i = 0; i < count; i++) {
    const even = i % 2 === 0;
    const Q = removeEdge ? 1 : 0;
    if (even) {
      // prettier-ignore
      barycentric.push(
				0, 0, 1,
				0, 1, 0,
				1, 0, Q
			);
    } else {
      // prettier-ignore
      barycentric.push(
				0, 1, 0,
				0, 0, 1,
				1, 0, Q
			);
    }
  }

  // add the attribute to the geometry
  const array = new Float32Array(barycentric);
  const attribute = new BufferAttribute(array, 3);
  bufferGeometry.setAttribute("barycentric", attribute);
}
