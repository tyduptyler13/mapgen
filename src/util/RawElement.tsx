import React, { useMemo } from "react";

interface RawElementProps {
  children: HTMLElement;
}

/**
 * For rendering elements that don't exist in react.
 * @param children
 * @constructor
 */
export const RawElement: React.FC<RawElementProps> = ({ children }) => {
  return useMemo(
    () => <div ref={(ref) => ref?.appendChild(children)}></div>,
    [children],
  );
};
