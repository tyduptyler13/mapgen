import React, { useCallback, useRef, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  debounce,
  FormControl,
  InputLabel,
  MenuItem,
  Popover,
  Select,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
} from "@mui/material";
import { ColorizedAttribute, useMapGenerator } from "@/renderer";
import { RawElement } from "@/util/RawElement";
import { AltitudeScales } from "@worker/types";
import { ColorMap, colorMapIndexToColor } from "@/materials/ColorMaps";
import MenuIcon from "@mui/icons-material/Menu";
import AdsClickIcon from "@mui/icons-material/AdsClick";
import BugReportIcon from "@mui/icons-material/BugReport";
import SliderInput from "@/components/SliderInput";

type Feature = "tracker" | "debug";

const Map: React.FC = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  const [showMenu, setShowMenu] = useState(false);
  const menuAnchor = useRef<HTMLButtonElement>(null);

  const [enabledFeatures, setEnabledFeatures] = useState<Array<Feature>>([]);
  const [size, setSize] = useState({ width: 100, height: 100 });
  const [scales, setScales] = useState<Required<AltitudeScales>>({
    minorScale: 4,
    majorScale: 30,
    distortionScale: 4,
  });
  const [colors, setColors] = useState({
    colorizedAttribute: ColorizedAttribute.height,
    colorMap: ColorMap.Parula,
  });
  const [seedError, setSeedError] = useState(false);
  const [realSeed, setRealSeed] = useState<string>("0");
  const [seed, setSeed] = useState<number>(0);

  const { stats, pointerHelper } = useMapGenerator(canvasRef, {
    width: size.width,
    height: size.height,
    colors,
    scales,
    seed,
    showDebug: enabledFeatures.indexOf("debug") !== -1,
    showTracker: enabledFeatures.indexOf("tracker") !== -1,
  });

  const debouncedSetSize = useCallback(debounce(setSize, 200), [setSize]);
  const debouncedSetScales = useCallback(debounce(setScales, 200), [setScales]);
  const debouncedSeed = useCallback(debounce(setSeed, 200), [setSeed]);

  return (
    <>
      <Box
        component="canvas"
        sx={{
          height: "100%",
          width: "100%",
        }}
        ref={canvasRef}
      />
      <RawElement>{stats}</RawElement>
      <Box
        style={{
          left: pointerHelper.position.x,
          top: pointerHelper.position.y,
        }}
        sx={{
          position: "fixed",
          color: "red",
          pointerEvents: "none",
          background: "black",
          visibility:
            enabledFeatures.indexOf("tracker") !== -1 ? "visible" : "hidden",
        }}
      >
        {pointerHelper.text}
      </Box>
      <Button
        aria-describedby="map-controls"
        variant="contained"
        onClick={() => setShowMenu(!showMenu)}
        sx={{
          position: "fixed",
          right: 15,
          top: 15,
          borderRadius: 28,
        }}
        ref={menuAnchor}
      >
        <MenuIcon />
      </Button>
      <Popover
        id="map-controls"
        open={showMenu}
        anchorEl={menuAnchor.current}
        onClose={() => setShowMenu(false)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <Card>
          <CardContent
            sx={{
              "& > *": {
                marginTop: "1rem !important",
              },
            }}
          >
            <Box>
              <ToggleButtonGroup
                value={enabledFeatures}
                onChange={(_, value) => {
                  setEnabledFeatures(value);
                }}
                orientation="horizontal"
              >
                <ToggleButton value="tracker" title="Show/Hide coordinates">
                  <AdsClickIcon />
                </ToggleButton>
                <ToggleButton value="debug" title="Show/Hide debug">
                  <BugReportIcon />
                </ToggleButton>
              </ToggleButtonGroup>
            </Box>
            <TextField
              value={realSeed}
              label="Seed"
              helperText={
                seedError
                  ? "Value must be an integer"
                  : "A number to feed the random generator"
              }
              error={seedError}
              onChange={(e) => {
                setRealSeed(e.target.value);
                if (e.target.value.match(/^\d+$/)) {
                  debouncedSeed(parseInt(e.target.value));
                  setSeedError(false);
                } else {
                  setSeedError(true);
                }
              }}
            />
            <SliderInput
              name="Width"
              initialValue={size.width}
              min={2}
              max={1000}
              onValueChange={(value) => {
                debouncedSetSize({ ...size, width: value as number });
              }}
            />
            <SliderInput
              name="Height"
              initialValue={size.height}
              min={2}
              max={1000}
              onValueChange={(value) => {
                debouncedSetSize({ ...size, height: value });
              }}
            />
            <SliderInput
              name="Minor noise scale"
              initialValue={scales.minorScale}
              min={1 / 4}
              max={20}
              step={0.1}
              onValueChange={(value) => {
                debouncedSetScales({
                  ...scales,
                  minorScale: value,
                });
              }}
            />
            <SliderInput
              name="Major noise scale"
              initialValue={scales.majorScale}
              min={1}
              max={100}
              onValueChange={(value) => {
                debouncedSetScales({
                  ...scales,
                  majorScale: value,
                });
              }}
            />
            <SliderInput
              name={"Distortion noise scale"}
              initialValue={scales.distortionScale}
              min={1}
              max={100}
              onValueChange={(value) => {
                debouncedSetScales({
                  ...scales,
                  distortionScale: value as number,
                });
              }}
            />

            <FormControl fullWidth>
              <InputLabel id="selected-attribute">
                Highlighted Attribute
              </InputLabel>
              <Select
                label="Highlighted Attribute"
                labelId="selected-attribute"
                value={colors.colorizedAttribute}
                onChange={(e) => {
                  setColors({
                    ...colors,
                    colorizedAttribute: e.target.value as ColorizedAttribute,
                  });
                }}
              >
                <MenuItem value={ColorizedAttribute.height}>Elevation</MenuItem>
                <MenuItem value={ColorizedAttribute.minorNoise}>
                  Minor Noise
                </MenuItem>
                <MenuItem value={ColorizedAttribute.majorNoise}>
                  Major Noise
                </MenuItem>
                <MenuItem value={ColorizedAttribute.majorNoise2}>
                  Major Noise 2
                </MenuItem>
              </Select>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel id="selected-color-map">Color Map</InputLabel>
              <Select
                label="Color Map"
                labelId="selected-color-map"
                value={colors.colorMap}
                onChange={(event) =>
                  setColors({
                    ...colors,
                    colorMap: event.target.value as unknown as ColorMap,
                  })
                }
              >
                {Array.from(
                  { length: Object.keys(ColorMap).length / 2 },
                  (_, id) => (
                    <MenuItem value={id} key={id}>
                      {colorMapIndexToColor[id as unknown as ColorMap].name}
                    </MenuItem>
                  ),
                )}
              </Select>
            </FormControl>
          </CardContent>
        </Card>
      </Popover>
    </>
  );
};

export default Map;
