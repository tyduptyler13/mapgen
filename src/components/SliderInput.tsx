import { Box, Slider, SliderProps, Typography } from "@mui/material";
import React, { useState } from "react";

export const SliderInput = ({
  name,
  initialValue,
  onValueChange,
  ...props
}: {
  name: string;
  initialValue: number;
  onValueChange: (value: number) => void;
} & SliderProps) => {
  const [value, setValue] = useState<number>(initialValue);

  return (
    <Box>
      <Typography id="input-slider" gutterBottom>
        {name}: {value}
      </Typography>
      <Slider
        {...props}
        value={value}
        onChange={(_, value) => {
          setValue(value as number);
          onValueChange(value as number);
        }}
      />
    </Box>
  );
};

export default SliderInput;
