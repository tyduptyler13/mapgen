import { Color, DoubleSide, ShaderMaterial } from "three";
import barycentricFrag from "@glsl/barycentric.frag";
import barycentricVert from "@glsl/barycentric.vert";

export interface BarycentricMaterialProps {
  dashAnimate?: boolean;
  dashEnabled?: boolean;
  dashLength?: number;
  dashOverlap?: boolean;
  dashRepeats?: number;
  dualStroke?: boolean;
  insideAltColor?: boolean;
  noiseA?: boolean;
  noiseB?: boolean;
  secondThickness?: number;
  seeThrough?: boolean;
  squeeze?: boolean;
  squeezeMax?: number;
  squeezeMin?: number;
  stroke?: Color;
  thickness?: number;
  time?: number;
}

export class BarycentricMaterial extends ShaderMaterial {
  constructor({
    dashAnimate = false,
    dashEnabled = true,
    dashLength = 0.55,
    dashOverlap = false,
    dashRepeats = 2.0,
    dualStroke = false,
    insideAltColor = true,
    noiseA = false,
    noiseB = false,
    secondThickness = 0.05,
    seeThrough = false,
    squeeze = false,
    squeezeMax = 1.0,
    squeezeMin = 0.1,
    stroke = new Color(1, 1, 1),
    thickness = 0.01,
    time = 0,
  }: BarycentricMaterialProps) {
    super({
      extensions: {
        derivatives: true,
      },
      transparent: false,
      side: DoubleSide,
      uniforms: {
        dashAnimate: { value: dashAnimate ?? false },
        dashEnabled: { value: dashEnabled ?? true },
        dashLength: { value: dashLength ?? 0.55 },
        dashOverlap: { value: dashOverlap ?? false },
        dashRepeats: { value: dashRepeats ?? 2.0 },
        dualStroke: { value: dualStroke ?? false },
        insideAltColor: { value: insideAltColor ?? true },
        noiseA: { value: noiseA ?? false },
        noiseB: { value: noiseB ?? false },
        secondThickness: { value: secondThickness ?? 0.05 },
        seeThrough: { value: seeThrough ?? false },
        squeeze: { value: squeeze ?? false },
        squeezeMax: { value: squeezeMax ?? 1.0 },
        squeezeMin: { value: squeezeMin ?? 0.1 },
        stroke: { value: stroke ?? new Color(1, 1, 1) },
        thickness: { value: thickness ?? 0.01 },
        time: { value: time ?? 0 },
      },
      fragmentShader: barycentricFrag,
      vertexShader: barycentricVert,
    });
  }
}
