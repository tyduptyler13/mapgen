import {
  BufferAttribute,
  Clock,
  Color,
  DoubleSide,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneGeometry,
  Points,
} from "three";
import { BarycentricMaterial } from "@/materials";
import {
  AltitudeScales,
  GenerateMapRequest,
  isTriangulationResponse,
  RegenerateAltitudeRequest,
  WorkerMessage,
} from "@worker/types";
import { RefObject, useEffect, useRef, useState } from "react";
import { lerp } from "three/src/math/MathUtils";
import { ColorMap, colorMapIndexToColor } from "@/materials/ColorMaps";
import useIntersector from "@/renderer/useIntersector";
import useMeshGeneration from "@/renderer/meshGeneration";
import useRenderer from "@/renderer/useRenderer";

export enum ColorizedAttribute {
  minorNoise,
  majorNoise,
  majorNoise2,
  height,
}

export function useMapGenerator(
  canvasRef: RefObject<HTMLCanvasElement | null>,
  {
    width,
    height,
    seed,
    colors,
    scales,
    showDebug = false,
    showTracker = false,
  }: {
    width: number;
    height: number;
    colors: {
      colorizedAttribute: ColorizedAttribute;
      colorMap: ColorMap;
    };
    scales: AltitudeScales;
    seed?: number | number[];
    showDebug?: boolean;
    showTracker?: boolean;
  },
) {
  const { stats, scene, camera, addPostRenderHook, removePostRenderHook } =
    useRenderer({ canvasRef });

  const colorsRef = useRef(colors);

  const { current: clock } = useRef(new Clock(true));
  const setColorsRef = useRef<(() => void) | null>(null);

  const { current: barycentricMaterial } = useRef(
    new BarycentricMaterial({
      dashAnimate: true,
    }),
  );

  useEffect(() => {
    const updateBarycentricMaterialUniforms = () => {
      barycentricMaterial.uniforms["time"].value = clock.getElapsedTime();
    };
    addPostRenderHook(updateBarycentricMaterialUniforms);

    return () => removePostRenderHook(updateBarycentricMaterialUniforms);
  }, []);

  const meshRef = useRef<Mesh | null>(null);
  const verticalStitchRef = useRef<Mesh | null>(null);
  const horizontalStitchRef = useRef<Mesh | null>(null);
  const [holes, setHoles] = useState<Points | null>(null);

  const { generateMesh, generatePointMesh, generateStitchMesh } =
    useMeshGeneration({ width, height, barycentricMaterial });

  useEffect(() => {
    const waterPlane = new Mesh(
      new PlaneGeometry(width * 3, height * 3).rotateX(Math.PI / 2),
      new MeshBasicMaterial({
        color: new Color("blue"),
        side: DoubleSide,
        opacity: 0.4,
        transparent: true,
      }),
    );
    scene.add(waterPlane);
    return () => {
      waterPlane.removeFromParent();
    };
  }, [width, height]);

  function workerListener({ data }: MessageEvent) {
    if (isTriangulationResponse(data)) {
      const mesh = generateMesh(meshRef, data.positionBuffer);

      const verticalStitchMesh = generateStitchMesh(
        verticalStitchRef,
        data.verticalStitch,
      );
      const horizontalStitchMesh = generateStitchMesh(
        horizontalStitchRef,
        data.horizontalStitch,
      );

      let dummy = new Object3D();
      let i = 0;
      for (let x = 0; x <= 1; ++x) {
        for (let y = -1; y <= 1; ++y) {
          dummy.position.set(x * width - height, 0, y * height);
          dummy.updateMatrix();
          verticalStitchMesh.setMatrixAt(i++, dummy.matrix);
        }
      }
      i = 0;
      for (let x = -1; x <= 1; ++x) {
        for (let y = 0; y <= 1; ++y) {
          dummy.position.set(x * width, 0, y * height - width);
          dummy.updateMatrix();
          horizontalStitchMesh.setMatrixAt(i++, dummy.matrix);
        }
      }

      scene.add(generatePointMesh(holes, setHoles, data.holes));

      // Using closures around the data because react really doesn't care for these huge arrays
      setColorsRef.current = function setColors() {
        let attribute;
        switch (colorsRef.current.colorizedAttribute) {
          case ColorizedAttribute.minorNoise:
            attribute = data.minorNoise;
            break;
          case ColorizedAttribute.majorNoise:
            attribute = data.majorNoise;
            break;
          case ColorizedAttribute.majorNoise2:
            attribute = data.majorNoise2;
            break;
          case ColorizedAttribute.height:
            attribute = data.height;
            break;
          default:
            throw new Error("Unknown colorized attribute");
        }

        if (!attribute) {
          console.warn("Color array is empty!");
        }

        const colors = new Float32Array(attribute.length * 3);
        const map = colorMapIndexToColor[colorsRef.current.colorMap];
        const range = map.r.length - 1;

        for (let i = 0; i < attribute.length; i++) {
          const scaled = attribute[i] * range;
          const floor = Math.floor(scaled);
          const ceil = Math.ceil(scaled);
          const remainder = scaled % 1;
          colors[i * 3] = lerp(map.r[floor], map.r[ceil], remainder);
          colors[i * 3 + 1] = lerp(map.g[floor], map.g[ceil], remainder);
          colors[i * 3 + 2] = lerp(map.b[floor], map.b[ceil], remainder);
        }

        mesh.geometry.setAttribute("color", new BufferAttribute(colors, 3));
      };

      setColorsRef.current();

      // scene.add(verticalStitchMesh);
      // scene.add(horizontalStitchMesh);
      scene.add(mesh, verticalStitchMesh, horizontalStitchMesh);
    } else {
      console.error("Unknown message!", data);
    }
  }

  useEffect(() => {
    if (holes) {
      holes.visible = showDebug;
    }

    if (verticalStitchRef.current) {
      verticalStitchRef.current.visible = showDebug;
    }

    if (horizontalStitchRef.current) {
      horizontalStitchRef.current.visible = showDebug;
    }
  }, [
    holes,
    verticalStitchRef.current,
    horizontalStitchRef.current,
    showDebug,
  ]);

  const [worker, setWorker] = useState<Worker>();
  useEffect(() => {
    const worker = new Worker(new URL("@worker", import.meta.url));

    setWorker(worker);

    worker.addEventListener("message", workerListener);

    return () => worker!.terminate();
  }, [width, height, seed]);

  useEffect(() => {
    if (!worker) {
      return;
    }

    const initialRequest: GenerateMapRequest = {
      messageType: WorkerMessage.GenerateMapRequest,
      maxWidth: width,
      maxHeight: height,
      seed,
      scales,
    };

    worker.postMessage(initialRequest);
  }, [worker, width, height, seed]);

  const previousScalesRef = useRef<AltitudeScales | null>(null);
  useEffect(() => {
    if (!worker || scales === previousScalesRef.current) {
      return;
    }

    previousScalesRef.current = scales;

    const request: RegenerateAltitudeRequest = {
      messageType: WorkerMessage.RegenerateAltitudeRequest,
      maxWidth: width,
      maxHeight: height,
      seed,
      scales,
    };

    worker.postMessage(request);
  }, [worker, scales]);

  useEffect(() => {
    colorsRef.current = colors;
    setColorsRef.current?.();
  }, [colors]);

  const { pointerHelper } = useIntersector({
    camera,
    mesh: meshRef.current,
    addPostRenderHook,
    removePostRenderHook,
    enabled: showTracker,
  });

  return {
    stats: stats.dom,
    pointerHelper,
  };
}
