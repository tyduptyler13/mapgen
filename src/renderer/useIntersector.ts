import { useEffect, useState } from "react";
import { Mesh, PerspectiveCamera, Raycaster, Vector2 } from "three";

export default function useIntersector({
  enabled,
  camera,
  mesh,
  addPostRenderHook,
  removePostRenderHook,
}: {
  enabled: boolean;
  camera: PerspectiveCamera | null;
  mesh: Mesh | null;
  addPostRenderHook: (hook: () => void) => void;
  removePostRenderHook: (hook: () => void) => void;
}) {
  const [pointerHelper, setPointerHelper] = useState({
    text: "",
    position: { x: 0, y: 0 },
  });

  useEffect(() => {
    if (!mesh || !camera) {
      return;
    }

    if (!enabled) {
      setPointerHelper({
        text: "",
        position: { x: 0, y: 0 },
      });
      return;
    }

    let pointerRef = {
      eventPos: { x: 0, y: 0 },
      normalizedPos: { x: 0, y: 0 },
    };

    const listener = (event: MouseEvent) => {
      pointerRef = {
        eventPos: event,
        normalizedPos: {
          x: (event.x / window.innerWidth) * 2 - 1,
          y: -((event.y / window.innerHeight) * 2 - 1),
        },
      };
    };
    window.addEventListener("mousemove", listener);

    const raycaster = new Raycaster();

    const renderHook = () => {
      raycaster.setFromCamera(
        new Vector2(pointerRef.normalizedPos.x, pointerRef.normalizedPos.y),
        camera,
      );

      const intersections = raycaster.intersectObject(mesh);

      if (intersections.length > 0) {
        setPointerHelper({
          text: JSON.stringify(intersections[0].point),
          position: pointerRef.eventPos,
        });
      }
    };

    addPostRenderHook(renderHook);

    return () => {
      removePostRenderHook(renderHook);
      window.removeEventListener("mousemove", listener);
    };
  }, [camera, mesh, enabled]);

  return { pointerHelper };
}
