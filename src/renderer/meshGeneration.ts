import { Dispatch, MutableRefObject, useMemo } from "react";
import {
  BufferAttribute,
  BufferGeometry,
  InstancedMesh,
  Mesh,
  Object3D,
  Points,
  PointsMaterial,
} from "three";
import { applyBarycentricAttribute } from "@/util/barycentricGeometryHelper";
import { BarycentricMaterial } from "@/materials";

export default function useMeshGeneration({
  width,
  height,
  barycentricMaterial,
}: {
  width: number;
  height: number;
  barycentricMaterial: BarycentricMaterial;
}) {
  return useMemo(() => {
    function generateMesh(
      meshRef: MutableRefObject<Mesh | null>,
      positionBuffer: Float32Array,
    ): Mesh {
      const geometry = new BufferGeometry();
      geometry.setAttribute("position", new BufferAttribute(positionBuffer, 3));

      geometry.computeBoundingBox();
      applyBarycentricAttribute(geometry);

      const mesh = new InstancedMesh(geometry, barycentricMaterial, 9);

      let dummy = new Object3D();
      let i = 0;
      for (let x = -1; x <= 1; ++x) {
        for (let y = -1; y <= 1; ++y) {
          dummy.position.set(x * width, 0, y * height);
          dummy.updateMatrix();
          mesh.setMatrixAt(i++, dummy.matrix);
        }
      }

      meshRef.current?.removeFromParent();
      meshRef.current = mesh;

      return mesh;
    }

    function generatePointMesh(
      obj: Points | null,
      setter: Dispatch<Points | null>,
      points: Float32Array,
    ): Points {
      const geometry = new BufferGeometry();
      geometry.setAttribute("position", new BufferAttribute(points, 3));
      geometry.computeBoundingBox();

      const material = new PointsMaterial({ depthTest: false });

      const mesh = new Points(geometry, material);

      mesh.position.set(-width / 2, 0, -height / 2);

      obj?.removeFromParent();
      setter(mesh);

      return mesh;
    }

    function generateStitchMesh(
      stitchRef: MutableRefObject<Mesh | null>,
      positionBuffer: Float32Array,
    ): InstancedMesh {
      const geometry = new BufferGeometry();
      geometry.setAttribute("position", new BufferAttribute(positionBuffer, 3));

      geometry.computeBoundingBox();
      applyBarycentricAttribute(geometry);

      const mesh = new InstancedMesh(geometry, barycentricMaterial, 6);

      stitchRef.current?.removeFromParent();
      stitchRef.current = mesh;

      return mesh;
    }

    return {
      generateMesh,
      generatePointMesh,
      generateStitchMesh,
    };
  }, [width, height, barycentricMaterial]);
}
