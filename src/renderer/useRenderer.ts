import { RefObject, useCallback, useEffect, useMemo, useRef } from "react";
import { PerspectiveCamera, Scene, WebGLRenderer } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import Stats from "three/examples/jsm/libs/stats.module";

export default function useRenderer({
  canvasRef,
}: {
  canvasRef: RefObject<HTMLCanvasElement | null>;
}) {
  const { scene, stats, camera } = useMemo(() => {
    const camera = new PerspectiveCamera(
      60,
      window.innerWidth / window.innerHeight,
      0.001,
      10000,
    );
    camera.position.set(40, 20, 0);

    return {
      scene: new Scene(),
      stats: new Stats(),
      camera,
    };
  }, []);
  const postRenderHooks = useRef<Array<() => void>>([]);

  useEffect(() => {
    if (!canvasRef.current) {
      return;
    }

    const renderer = new WebGLRenderer({
      antialias: true,
      canvas: canvasRef.current,
      logarithmicDepthBuffer: true,
    });

    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.listenToKeyEvents(window);

    const handleResize = () => {
      renderer.setSize(window.innerWidth, window.innerHeight);
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
    };
    handleResize();

    addEventListener("resize", handleResize);

    let cancelled = false;
    const handleAnimationFrame = () => {
      stats.begin();

      renderer.render(scene, camera);

      stats.end();

      controls.update();

      postRenderHooks.current.forEach((it) => it());

      if (!cancelled) {
        requestAnimationFrame(handleAnimationFrame);
      }
    };

    requestAnimationFrame(handleAnimationFrame);

    return () => {
      // Cleanup
      cancelled = true;
      removeEventListener("resize", handleResize);
      controls.dispose();
    };
  }, [canvasRef.current]);

  const addPostRenderHook = useCallback(
    (hook: () => void) => {
      postRenderHooks.current.push(hook);
    },
    [postRenderHooks.current],
  );

  const removePostRenderHook = useCallback(
    (hook: () => void) => {
      const index = postRenderHooks.current.indexOf(hook);

      if (index === -1) {
        throw new Error("[Not found] The hook was not found in the array!");
      }

      postRenderHooks.current.slice(index, 1);
    },
    [postRenderHooks.current],
  );

  return {
    addPostRenderHook,
    removePostRenderHook,
    stats,
    scene,
    camera,
  };
}
