import React from "react";
import { Box, CssBaseline } from "@mui/material";
import Map from "./components/Map";

const App: React.FC = () => {
  return (
    <>
      <CssBaseline />
      <Box sx={{ display: "flex" }}>
        <Map />
      </Box>
    </>
  );
};

export default App;
