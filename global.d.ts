import { MaterialNode } from "@react-three/fiber";
import { BarycentricMaterial } from "@/materials/BarycentricMaterial";

declare module "@react-three/fiber" {
  export interface ThreeElements {
    barycentricMaterial: MaterialNode<
      BarycentricMaterial,
      typeof BarycentricMaterial
    >;
  }
}
