Thoughts for features:

- Infinite world using tiling squares
  - Requires customizing delaunator or its output
- Circular worlds with oceans for edges (flat earth)
  - would be column like as we still want mining
- Towns/features can spawn more terrain nodes around their edges to allow for easier transitions to the neighboring nodes

Game ideas:

- City management on a way more macro scale (intra city sim)
- Huge battlefield like planetside with wvw elements from guild wars
  - Use mercenary design to balance teams (if one side is overloaded, instant spawns for mercs for the team with the smaller pop)

Game element ideas:

- Mage vs machine
- nature vs man
- rpg
  - Hardcore classes?
  - Reward players for allowing a truly random starting roll (like dnd) and reward them for playing to their design
